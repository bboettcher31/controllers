The Bionic Harp
===============

**A CIRMMT Student Award Project for 2019/2020**

John Sullivan  
Alex Tibbitts

**_The Bionic Harp_** is a new research-creation project that extends our previous work on harp gesture acquisition for the control of audiovisual synthesis. This new project moves in a different direction, towards the creation and evaluation of a new musical interface for the concert harp using a user-centered participatory design approach. Where previously we had focused on motion analysis of free-handed gestures for the control of extra-musical parameters during performance, this project will work to directly augment the harp with a custom sensor interface that is physically coupled with the instrument, thereby transforming the acoustic concert harp into a "bionic harp". The interface will provide the harpist with an ergonomic set of controls to blend their natural instrumental performance with additional computer-based synthesis and effects processing. 

*See [docs/meething_notes](docs/meeting_notes) folder for notes (1 doc per session)*

----

### Basic project overview and schedule

![whiteboard1_overview](media/2019-09-11_whiteboard1.jpg)

### Interface design ideas

![whiteboard2_interface_ideas](media/2019-09-11_whiteboard2.jpg)

### CAD designs

![CAD design perspective](media/Bionic_Harp_CAD_perspective.jpg)  
![CAD design top](media/Bionic_Harp_CAD_top.jpg)

----

## Goals and methods of research project

"The Bionic Harp" is a new research-creation project that extends the work of a previous successful CIRMMT Student Award-sponsored project, "Harp gesture acquisition for the control of audiovisual synthesis" (2016/17). That three-phase project comprised a motion capture study of gesture in harp performance, the design of wearable hardware controllers to integrate motion control into live instrumental performance, and the creation and premiere of a new work for solo harp and electronics by harpist and team member Alexandra Tibbitts (composed by Brice Gatinet). The current proposed project intends to move in a new direction towards the design and evaluation of a new musical interface for the concert harp. Where the previous project focused on free-handed gesture for the control of extra-musical parameters during performance, the resulting wearable hardware was not instrument specific. This proposed project will work to directly augment the harp with a custom sensor interface that is physically coupled with the instrument, thereby transforming the acoustic concert harp into a "bionic harp", providing the harpist with an ergonomic set of controls to interface their instrumental performance with additional computer-based synthesis and effects parameters. 

The methodology for this work is fundamentally based on a user-centered participatory design approach. Working with Tibbitts (now a CIRMMT collaborator), I will experiment with prototypes based on the input and feedback of an experienced professional harpist. Throughout the design process I will also organize a design session with additional harpists to get rapid and direct feedback on the experimental prototypes which will guide the process towards an optimized final version. When the interface is completed, I will hold a session with a group of harpists to evaluate the finalized interface in terms of its potential success as a viable performance tool. 

Finally, this application is also coordinated with a live@CIRMMT application being submitted by Alexandra Tibbitts, for a performance of a new work for the Bionic Harp that she will prepare. 

There is evidence of increased attention towards the development new tools for augmented harp performance, with individuals like Una Monaghan (contemporary harpist and visiting CIRMMT researcher, 2016) and a new collaborative project between composer Arnaud Petit (CIRMMT Distinguished lecturer, 2007), Dr. James Leonard (Université Grenoble Alpes) and Camac Harps. While there is no guarantee that the work proposed here will directly interface with these other researchers and projects, it is situated in an area of active research and growing interest.


----

## Research plans for the coming year

The main activity of this project will be the collaborative design and evaluation of a new control interface for the concert harp, along with accompanying software to integrate the control data into a live performance workflow. Harpist Alexandra Tibbitts will be a important collaborator in the project, in the roles of musician/co-designer and primary end user. Additionally, during the design and evaluation portions of the project, a group of area harpists will also be invited to test the new interface and give feedback on its design. The results of the project will be disseminated through publication at its conclusion. More immediately, the finished design will be put to use in Tibbitts' performance practice, with hopes of its inclusion in a live@CIRMMT concert.

The projected project timeline for the coming year is as follows: 

- Summer 2019
    + Review of motion capture study from previous project. Potential deeper analysis or re-analysis of data as needed to inform new augmented harp design. (1 month)
    + Experimentation with simple prototypes of sensor interfaces, selection of sensor technologies, 3-D modeling, sketches and non-functional prototyping of control interface ideas. (1 month)
- Fall 2019
    + Iterative co-design of augmented harp interface prototypes and software, with  Tibbits. (3 months)
    + Testing and evaluation of prototypes with Tibbitts. (concurrent with previous item)
    + Group workshop with harpists to test and give feedback on multiple interface prototype ideas. (1-day session)
- Spring 2020
    + Finalize harp interface and production. (2 months)
    + 2nd group workshop with harpists to test and evaluate final version. (1-day session)
- Spring 2020
    + Performance with the new augmented harp at a live@CIRMMT concert (if application is accepted - if not then performance will be scheduled at an alternate venue). (1 day)
    + Presentation of finished project for CIRMMT Student Symposium. 
- Summer 2020
    + Submission and publication of project paper to appropriate journal or conference (target journals: Computer Music Journal, Organised Sound; potential conferences: NIME, ICMC, SMC).

