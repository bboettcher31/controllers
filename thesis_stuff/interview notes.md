*Start at 1:58*

## Background/Survey questions

**Can you describe your performance practice? What style(s) of music do you play, frequency and type of performances you give, ensemble or solo performance, instruments/setups?**

- classically trained harpist
- interested in working with composers, incorporating electronics into new music, started with working with others who would add an electronic layer that wouldn't have any input into creation of. Wanted to be more involved in the creation process. 
- Working with Kevion Gironnay - working with aleatoric music and improvisation. 

> Trying to move to a more autonomous position where I'm not just a performer but I become the artistic creator

- Styles: 
    - Experimental improvisation with electronics
- Ensembles/solo?
    - Both
    - Ensemble: mild processing with laptop; other members have unique setups mixing conventional instruments and electronics
- Performance frequency:
    - mix between performances and recordings
    - avg. 1 per week, between classical/paid work and experimental
    - formerly more classical, but now doing more with experimental

## Current project, the Bionic Harp: 

**Our current project, The Bionic Harpist, has gone through a full design cycle, from ideation, then building and evaluating various prototypes (non-functional, lo-fi, digital, etc.), fabrication and testing, to employing them in artistic performances. What has your experience been through it?**

- Started with vague ideas; end result looks totally different. 
- End result is more ergonomic, and hadn't thought of that at the start
- Storyboarding and constant check-ins were important. Was good to take the time to put lots of ideas out there, and it was important to communicate clearly with each other, to convey and understand which ideas were important adn which direction we were going to work in. Have seen other collaborations that didn't have the same common vision that ended poorly.
- Getting used to working with different tools. Digital prototyping with TouchOSC interfaces were important (able to try real working prototypes out, give feedback and make updates immediately). Building autonomy as a performer.
- At the end, blank space on right controller, I made executive decision to add light-up button grid, which became the most important part of the controllers. Need the feedback that the lights provide. 

**What were some high points of the process? Low points?**

- Harder to think about the low things - we were open minded about the process to see what works. 
- Low points: 
    - System still can't be adopted to other harps. We have a theory of how to approach it, but not there. 
    - A lot of lessons to learn and adaptation. 

**You have now used the controllers in performance a couple times, including a high profile concert at MUTEK. How have you been able to transition between design and performance modes? (Can comment on Avatar residency as well, a 1-month residency to develop the MUTEK performance)**

> I have to say that, I have a lot of ideas in my head, especially before starting this project. But as you really pointed out:
> **There was a lot of learning about how to be an artist, while also creating the tool that I want to make my art with.**
> So it set up a lot of challenges and, challenges are great, and a lot of risk taking. And so I felt like I spent this last year learning a lot of the foundations behind what it is to create and what my voice is trying to go towards. Your first question was what styles does my music lie in, and my answer was experimental , but that's not where I want to stay, so I think slowly throughout each step of this project form premiering the  controllers in February at CIRMMT, where it was just me doing some light improvisation, but very shy, to going to MUTEK and taking a bow and thrashing the harp and making a spectacle out of the whole image itself. So I know I made a huge transformation, just between those February and September snapshots. 
> But it's still not the end of where I want this project to go, so I feel like I've been really lucky to have our collaboration where we're building these controllers at the same time as I'm building my artistic practice on a more independent scale. 

## Previous collaboration

**This was our second collaborative project to design performance interfaces together. What are your recollections of the first project, that moved through mocap analysis, interface design, and development and performance of a new creative work?** 

- Looking at gesture would be a good starting point. 
- Lucky to have CIRMMT, and it was Alex's first research project, good experience learning how to run a project with human subjects and work as project coordinator, has developed that skill since in professional work
- Learned the bugs of collaboration with this project. Started with an  idea, but in terms of an aesthetic (creative application) for the controllers it was open ended, between Alex and Brice (composer). 
- Had wanted to use the controllers to track the natural harp playing motions, but in the piece the controllers were not used that way; instead created new gestures. In Romantic era, 'word painting' was a technique that was developed, so interested in extending this to gesture and movement. 
- Haven't worked with controllers recently, but would like to explore that more -> natural gestures

**Do you feel like there has been a continuation between the two projects, and if so, in what ways?** 

- Because of first project, people expect Alex to be working specifically with gesture, but not really interested in that with Bionic Harp controllers.
- But the question is: "How do you make an interesting performance" with Bionic Harp controllers, how to create an interesting visual performance (the problem with DJs)
- Interested in putting controls on the harp so control of electronics is onboard and integrated with harp playing
- Reviewing MUTEK videos and considering gestures used, creating more practiced and fluid gestures. 
- (me): in our first project we each had our own specific research goals: me: mocap, project management; Alex: electronic performance; Olafur: hardware motion controllers; Brice: electronic composition. 

## Workshop participation

**Prior to beginning our second project, you were a participant in the 'Design for Performance' workshop. What was your experience of the workshop?**  
**Are there things about the workshop that you felt worked well?**  

- Was run before Bionic Harp project. 
- It was an awesome experience because hadn't thought of design as a "fun, arts and crafts" project. 
- "I'm not good at arts and crafts, so I didn't feel like my project was a good representation" but that's ok, almost kind of the point, really. 
- Interesting experience in trying to put ideas into a physical representation. 
  
**Are there things that you think could have been done differently or could be improved if it was run again?**

- Only that we never had a follow up. It would have been really nice to see what direction these ideas went in. It was worthy of pursuing further. 

**Were there elements of the workshop that contributed to the Bionic Harp project?**

- Gave an insight into how we could start planning. Started with sketches of harp and ideas of what she wanted. 

> It gave me that artistic licence to start thinking of it, because you kind of demystified that idea of what it is to build, because all of the sudden...

> This process of crafting it (the workshop) gave me a way of expressing to you what I needed, and made me feel like it was possible to do, and your enthusiastic collaboration made it a project that could come to life adn that I didn't have to feel bad that I don't have the experience as a programmer, 3D modeler, etc.

- (me): also we used some of the same exact tools and methods from the workshop - non-functional prototyping. And that led to one of the biggest design breakthroughs of the project: the angled control panels. 

**In looking back at the design artefacts from the workshop, your prototype bears some resemblance to the Bionic Harp controllers we ended up building. Did you have that in the back of your mind?**

- Already had some basic idea of a basic harp interface, but during the workshop activity, spent the first half of the time trying to build something completely different and new, and didn't really make sense. Then spent the last few minutes focused on just building the preexisting idea of the controllers. 
- Was easier to build and explain, because had already been actively thinking about previously. 

## Longevity and future work
**Have you continued to use the controllers that were designed in the first project? If not, why?** 

- Thought of using it for MUTEK, but realities of time, plus the idea of debuting two different new controllers at one time was a bit much.
- Interested to bring back the Genki Wave to explore some of own interest in natural performance gestures, and want to bring in along with the Bionic interfaces, to bring in a "tighter package of augmented instrument package"
- Not using some controllers/hardware now, but they are around and available and will be brought back in as the aesthetic calls for. 
- But also, part of the idea with the Bionic Harp controller is that it replaces a lot of other tools/interfaces in everyday performance workflow - all of the tools needed are on the harp itself. 

> But the whole idea with these controllers was that it takes a lot of those extra tools out of my every day workflow, because all the tools I need are on the harp itself.

**I know you intend to continue work on the Bionic Harp project, in terms of both ongoing technical and creative development. What are your motivations and expectations for both (technical and creative)?**

- Technical development: 
    - With the controllers, want to focus on what controls are necessary and unnecessary. With the first version, we used inexpensive components (eg pots and buttons), but now we know that (visual) feedback is necessary on the instrument. Working without computer, so need to look at controllers and know the state. So that is important, for example, use buttons with LEDs, maybe rotary encoders with visual feedback, perhaps a screen. The Adafruit Trellis grid with multicolor buttons were the most important controls of the current controllers. 
    - Need to figure out how to fit to other harps easily to accommodate touring.
- Creative development: 
    - Develop into a touring show
    - Actively searching for new venues to play, new collaborations with other electronic artists, especially bringing to different scenes, clubs, raves, etc.
    - "Want to bring the harp into venues that they're not really used to seeing a more classical-stigmatized instrument"
    - (me): Also working more with visuals as well, as shown in MUTEK

**Do you anticipate arriving at a point that the controllers/performance setup (including software/etc.) is "finished"? Or always in constant development/evolution?**

> I've learned that, if you are always making changes, it makes it harder to remember what you are creating. So I think it's very important to have versions that stay the same for blocks of time. And then you use that and you exhaust it until you really can't push your practice farther without updates.

- The idea is to have the next version of the controllers be good for 5-10 years. 
> We're always making advancements, and things are getting smaller and more efficient; also just the matter of how you plug things in, becomes harder to find the new adapters to work with your [older] electronics. So just understanding the reality of that itself, being open to advancements when they are necessary but not as a competition to always show the new hot technology on the market. 
> 
> I think it's much better to create something that's reliable and that is interesting itself, and we've already kind of checked the boxes there. I'm definitely in the place now that I want to get better at my craft, and exhaust my options... happily.

**Any reflections/comments on our cumulative collaboration, which has now stretched over 5 years?**

- Have some to value interdisciplinary collaboration. It's not easy with every artist and creator. 
- We've also had separate projects too. Also let the space take its place, so that was when we are ready to collaborate on the next project we are ready and refreshed adn ready to take it on and move to the next things. 
- We've both worked tons in between the collaborations, so we both come back with new skills, tools, ideas, etc., that we can bring. 

*End at 48:03*