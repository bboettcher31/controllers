Interview questions for Alex Tibbitts
=====================================

## Context: 

Alex is a harpist who I have been collaborating with for the last few years. We began by performing a piece for harp and electronics called "Techno Concerto" by Caroline Lizotte (2016). Our first research project together was a CIRMMT Student Award funded project called "Gestural Control of Augmented Instrumental Performance: A Case Study of the Concert Harp" (2017-18) along with two other collaborators, in which we conducted a motion capture study of harp performance and developed tools for implementing gestural control of computer effects processing into Alex's solo live performance. After that project, Alex participated in the 'Design for Performance' workshop (2019). Most recently, we have collaborated on a new project to develop wireless controllers that attach to a concert harp, which have been successfully used in professional performance (2019-20). 

<!-- This interview will discuss our current project (the main focus of thesis Ch. 5) as well as our previous collaboration and participation in the Design workshop. We will discuss longevity (in terms of long-term DMI use/engagement), and connections and overlap between design research and artistic practice, as well as the prospects for new interfaces to be used in professional performance.  -->

-------

## Questions

### Background/Survey questions

- Can you describe your performance practice? What style(s) of music do you play, frequency and type of performances you give, ensemble or solo performance, instruments/setups?


### Current project, the Bionic Harp: 

- Our current project, The Bionic Harpist, has gone through a full design cycle, from ideation, then building and evaluating various prototypes (non-functional, lo-fi, digital, etc.), fabrication and testing, to employing them in artistic performances. What has your experience been through it?
- What were some high points of the process? Low points? 
- You have now used the controllers in performance a couple times, including a high profile concert at MUTEK. How have you been able to transition between design and performance modes? (Can comment on Avatar residency as well, a 1-month residency to develop the MUTEK performance)

### Previous collaboration

- This was our second collaborative project to design performance interfaces together. What are your recollections of the first project, that moved through mocap analysis, interface design, and development and performance of a new creative work? 
- Do you feel like there has been a continuation between the two projects, and if so, in what ways? 

### Workshop participation

- Prior to beginning our second project, you were a participant in the 'Design for Performance' workshop. Were there elements of the workshop that contributed to the Bionic Harp project? 
- Are there things about the workshop that you felt worked well?
- Are there things that you think could have been done differently or could be improved if it was run again? 

### Longevity and future work
- Have you continued to use the controllers that were designed in the first project? If not, why? 
- I know you intend to continue work on the Bionic Harp project, in terms of both ongoing technical and creative development. What are your motivations and expectations for both (technical and creative)?
- Any reflections/comments on our cumulative collaboration, which has now stretched over 4 years? 
- Do you anticipate arriving at a point that the controllers/performance setup (including software/etc.) is "finished"? Or always in constant development/evolution?

