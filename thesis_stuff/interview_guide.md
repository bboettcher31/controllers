Interview questions for Alex Tibbitts
=====================================

## Context: 

Alex is a harpist who I have been collaborating with for the last few years. We began by performing a piece for harp and electronics called "Techno Concerto" by Caroline Lizotte (2016). Our first research project together was a CIRMMT Student Award funded project called "Gestural Control of Augmented Instrumental Performance: A Case Study of the Concert Harp" (2017-18) along with two other collaborators, in which we conducted a motion capture study of harp performance and developed tools for implementing gestural control of computer effects processing into Alex's solo live performance. After that project, Alex participated in the 'Design for Performance' workshop (2019). Most recently, we have collaborated on a new project to develop wireless controllers that attach to a concert harp, which have been successfully used in professional performance (2019-20). 

This interview will discuss our current project (the main focus of thesis Ch. 5) and link together research from the previous chapters, with questions about performance practice and engagement from the EMI survey (Ch. 2), discussion about the design workshop (Ch. 3), and the previous and current collaborative design projects we have worked on as well as the implications and outcomes of the new interfaces use in professional performance. 

-------

## Questions

### Background/Survey questions

- Can you describe your performance practice? What style(s) of music do you play, frequency and type of performances you give, ensemble or solo performance, instruments/setups?
- Experience with computer programming or electronics?

### Discussion of previous and ongoing projects

- What were your original motivations for beginning our collaborative research on designing new interfaces for performance? 
- Can you discuss a little about our original project "Gestural control of augmented instrumental performance", in particular in terms of outcomes and implications for our subsequent work? It feels like we learned a lot and the process gave us several ideas for future work both in terms of what we would want to design and fruitful methods of working together. 
- Your participation in the 'Design for Performance' workshop was unique in that we had already completed one design-performance project and were discussing our future project. What was your experience of the workshop? Did you find it helpful in envisioning ideas for our future designs? 
- Our current project, The Bionic Harpist, feels like it has gone through a complete design cycle, starting with brainstorming, whiteboarding and sketching, then building several different prototypes (non-functional, lo-fi, digital, etc.), fabricating and testing the controllers, then using them directly in performance. What has your experience been with the process? 
- The work has culminated in a high profile performance at the MUTEK festival. Have the controllers filled your expectations?

### Harp controller evaluation

- What kinds of sounds or sound manipulations do you produce with it?
- What do you like about this instrument? 
- What do you dislike about it? 
- How reliable is it? Any recurrent hardware or software issues? 
- Is your instrument satisfactory in its present form, or would you like it to have different functionalities? 

### Engagement (from survey)

- What factors influence you to take up a new DMI/interface?
- On average, how long do you typically use a DMI/interface before retiring it? 
- What factors contribute to your long-term use of a DMI/interface? (Do you have any DMIs/interfaces that you continue to use? Genki Wave? nanoKontrol?) 
- What factors influence you to stop using a DMI/interface? 

### General exploratory topics for discussion (optional): 

- How to you view the relationship between the research adn artistic sides of your practice? Do you ever find them to be complementary or conflicting? 
- Since our original project together you have been using software extensively in your electroacoustic performance setup (Ableton Live and Max) and a mix of commercial and non-commercial controllers in combination with your acoustic instrument (harp). Can you discuss the contrast between hardware and software in your setups? Do you think of everything as a comprehensive instrument? Several instruments? Parts of various compositions? Do these distinctions matter?
- Future work (research and/or artistic)?
