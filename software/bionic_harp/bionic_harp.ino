/******************************
 * The Bionic Harp
 * Modular BLE MIDI controllers 
 * augmenting concrt harp
 * 
 * for the Wemos Lolin D32 PRO

 * 
 * http://gitlab.com/johnnyvenom-phd/bionic-harp
 * John D. Sullivan 
 * 
 ******************************/

#include "Adafruit_NeoTrellis.h"
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include <BLE2902.h>

#define MODULE 1  // 0 == LEFT
                  // 1 == RIGHT

#define SERVICE_UUID        "03b80e5a-ede8-4b33-a751-6ce34ec4c700"
#define CHARACTERISTIC_UUID "7772e5db-3868-4112-a1a9-f2669d106bf3"

#define MUX1 34
#define MUX2 32
#define MUX3 33
#define MUX4 34 // pin 34 on the RH module
#define LEDPIN 5

#define DELTIME 10

// digital select pins
#define S0 4
#define S1 0
#define S2 2

uint32_t thisVal = 0;

long prevMillis = 0;
long currMillis = 0;
bool ledState = false;

int r0 = 0; // value of select pin at the 4051 (s0)
int r1 = 0; // value of select pin at the 4051 (s1)
int r2 = 0; // value of select pin at the 4051 (s2)

int count = 0; // which y pin we are selecting

// MIDI values to send
uint8_t m1Val[] = {0,0,0,0,0,0,0,0};
uint8_t m2Val[] = {0,0,0,0,0,0,0,0};
uint8_t m3Val[] = {0,0,0,0,0,0,0,0};
uint8_t m4Val[] = {0,0,0,0,0,0,0,0};
uint8_t trVal[] = {0,0,0,0,0,0,0,0,
                   0,0,0,0,0,0,0,0};

// last input values
uint8_t m1Last[] = {0,0,0,0,0,0,0,0};
uint8_t m2Last[] = {0,0,0,0,0,0,0,0};
uint8_t m3Last[] = {0,0,0,0,0,0,0,0};
uint8_t m4Last[] = {0,0,0,0,0,0,0,0};
uint8_t trLast[] = {0,0,0,0,0,0,0,0,
                    0,0,0,0,0,0,0,0};

// MIDI CC#s
uint8_t m1cc[] = { // CC# 20 - 27
  0x14, 0x15, 0x16, 0x17,
  0x18, 0x19, 0x1a, 0x1b
};
uint8_t m2cc[] = { // CC# 28 - 35
  0x1c, 0x1d, 0x1e, 0x1f,
  0x20, 0x21, 0x22, 0x23
};
uint8_t m3cc[] = { // CC# 36 - 43
  0x24, 0x25, 0x26, 0x27,
  0x28, 0x29, 0x2a, 0x2b
};
uint8_t m4cc[] = { // CC# 44 - 51
  0x2c, 0x2d, 0x2e, 0x2f,
  0x30, 0x31, 0x32, 0x33
};
uint8_t trcc[] = { // CC# 52 - 67
  0x34, 0x35, 0x36, 0x37,
  0x38, 0x39, 0x3a, 0x3b,
  0x3c, 0x3d, 0x3e, 0x3f,
  0x40, 0x41, 0x42, 0x43
};

// input type (0 = momentary button, 1 = on/off, 2 = continuous)
uint8_t m1Type[] = {1,1,1,1,1,1,1,1};
uint8_t m2Type[] = {0,0,0,0,0,0,0,0};
uint8_t m3Type[] = {1,1,1,1,2,2,2,2};
uint8_t m4Type[] = {2,2,2,2,2,2,2,2};
uint8_t trType[] = {1,1,1,1,0,0,0,0,
                    0,0,0,0,0,0,0,0};

Adafruit_NeoTrellis trellis; // create a Trellis object

// *********** BLE-MIDI initialization stuff **********//

#define SERVICE_UUID        "03b80e5a-ede8-4b33-a751-6ce34ec4c700"
#define CHARACTERISTIC_UUID "7772e5db-3868-4112-a1a9-f2669d106bf3"

BLECharacteristic *pCharacteristic;
bool deviceConnected = false;

uint8_t midiPacket[] = {
  0x80, // header
  0x80, // timestamp, not implemented
  0xB0, // status byte (Continuous controller)
  0x00, // data byte 1 (Controller #)
  0x00, // data byte 2 (Controller value)
};

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      Serial.println("BLE device connected :) "); 
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
      Serial.println("BLE device disconnected :( ");
    }
};

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  if(MODULE == 0) {
    BLEDevice::init("Bionic_Harp_LH");
  } else {
    BLEDevice::init("Bionic_Harp_RH");
  }
    
  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(BLEUUID(SERVICE_UUID));

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
    BLEUUID(CHARACTERISTIC_UUID),
    BLECharacteristic::PROPERTY_READ   |
    BLECharacteristic::PROPERTY_WRITE  |
    BLECharacteristic::PROPERTY_NOTIFY |
    BLECharacteristic::PROPERTY_WRITE_NR
  );

  // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
  // Create a BLE Descriptor
  pCharacteristic->addDescriptor(new BLE2902());

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->addServiceUUID(pService->getUUID());
  pAdvertising->start();

  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);

  pinMode(LEDPIN, OUTPUT);
  pinMode(MUX1, INPUT);
  pinMode(MUX2, INPUT);
  pinMode(MUX3, INPUT);

  if (MODULE == 1) {

    if (!trellis.begin()) {
      Serial.println("Could not start trellis, check wiring?");
      while(1);
    } else {
      Serial.println("NeoPixel Trellis started");
    }
  
    //activate all keys and set callbacks
    for(int i=0; i<NEO_TRELLIS_NUM_KEYS; i++){
      trellis.activateKey(i, SEESAW_KEYPAD_EDGE_RISING);
      trellis.activateKey(i, SEESAW_KEYPAD_EDGE_FALLING);
      trellis.registerCallback(i, blink);
    }
  
    //do a little animation to show we're on
    for (uint16_t i=0; i<trellis.pixels.numPixels(); i++) {
      trellis.pixels.setPixelColor(i, Wheel(map(i, 0, trellis.pixels.numPixels(), 0, 255)));
      trellis.pixels.show();
      delay(50);
    }
    for (uint16_t i=0; i<trellis.pixels.numPixels(); i++) {
      trellis.pixels.setPixelColor(i, 0x000000);
      trellis.pixels.show();
      delay(50);
    }
  }
  
  Serial.println("The Bionic Harp"); 
  if (MODULE == 0) {
    Serial.println("LEFT module");
  } else {
    Serial.println("RIGHT module");
  }
  for(int i=0; i<10; i++){
    digitalWrite(LEDPIN, HIGH);
    Serial.print(". ");
    delay(100);
    digitalWrite(LEDPIN, LOW);
    delay(100);
  }
  Serial.println("");
}

void loop() {
  // put your main code here, to run repeatedly:

  currMillis = millis();
  if ( currMillis > prevMillis + 667) {
    if (ledState) {
      digitalWrite(LEDPIN, LOW);
      ledState = false;
    } else {
      digitalWrite(LEDPIN, HIGH);
      ledState = true;
    }
    prevMillis = currMillis;
  }

  if (MODULE == 0) {
    readMux(1); //MUX1_LH
    readMux(2); // MUX2_LH
    readMux(3); // MUX3_LH
  } else {
    readMux(4); // MUX4_RH
    readTrellis(); 
  }
  
  delay(DELTIME);
}

void readTrellis() {
  trellis.read(); 
  for(int count=0; count<16; count++) {
//    thisVal = trVal[count]; 
    if(trVal[count] != trLast[count]) {
      
      Serial.print("TREL-"); 
      Serial.print(count); 
      Serial.print(": "); 
      Serial.println(trVal[count]);   
      sendMIDImessage(trcc[count], trVal[count]);
      trLast[count] = trVal[count];
    }
  }
}

void readMux(int mux) {
  for(count=0; count<=7; count++) {

    r0 = bitRead(count, 0);
    r1 = bitRead(count, 1);
    r2 = bitRead(count, 2);

    digitalWrite(S0, r0);
    digitalWrite(S1, r1);
    digitalWrite(S2, r2);

    switch(mux) {
      case 1: // reading MUX1
        thisVal = translate(analogRead(MUX1), m1Type[count]);
        if(thisVal != m1Last[count]) { 
          
          // new button action (DO SOMETHING!!!)
          switch(m1Type[count]) {
            case 0: 
              // new momentary button state, update and send.
              m1Val[count] = thisVal;
              Serial.print("MUX1-"); 
              Serial.print(count); 
              Serial.print(": "); 
              Serial.println(m1Val[count]);
              sendMIDImessage(m1cc[count], m1Val[count]);
              break;
            case 1: 
              // toggle - check if current button val is on or off & update if needed
              if(thisVal == 127) {
                if(m1Val[count] == 0) {
                  m1Val[count] = 127;
                } else {
                  m1Val[count] = 0;
                }
                Serial.print("MUX1-"); 
                Serial.print(count); 
                Serial.print(": "); 
                Serial.println(m1Val[count]);   
                sendMIDImessage(m1cc[count], m1Val[count]);
              }
              break;
            case 2: 
              // continuous control, update and send
              m1Val[count] = thisVal;
              Serial.print("MUX1-");
              Serial.print(count);
              Serial.print(": ");
              Serial.println(m1Val[count]);
              sendMIDImessage(m1cc[count], m1Val[count]);
              break;
          }
          m1Last[count] = thisVal;
        }
        break;
      case 2: // reading MUX2 
        thisVal = translate(analogRead(MUX2), m2Type[count]);
        if(thisVal != m2Last[count]) { 
          
          // new button action (DO SOMETHING!!!)
          switch(m2Type[count]) {
            case 0: 
              // new momentary button state, update and send.
              m2Val[count] = thisVal;
              Serial.print("MUX2-"); 
              Serial.print(count); 
              Serial.print(": "); 
              Serial.println(m2Val[count]);
              sendMIDImessage(m2cc[count], m2Val[count]);
              break;
            case 1: 
              // toggle - check if current button val is on or off & update if needed
              if(thisVal == 127) {
                if(m2Val[count] == 0) {
                  m2Val[count] = 127;
                } else {
                  m2Val[count] = 0;
                }
                Serial.print("MUX2-"); 
                Serial.print(count); 
                Serial.print(": "); 
                Serial.println(m2Val[count]);   
                sendMIDImessage(m2cc[count], m2Val[count]);
              }
              break;
            case 2: 
              // continuous control, update and send
              m2Val[count] = thisVal;
              Serial.print("MUX2-");
              Serial.print(count);
              Serial.print(": ");
              Serial.println(m2Val[count]);
              sendMIDImessage(m2cc[count], m2Val[count]);
              break;
          }
          m2Last[count] = thisVal;
        }
        break;
      case 3: // reading MUX3
        thisVal = translate(analogRead(MUX3), m3Type[count]);
        if(thisVal != m3Last[count]) { 
          
          // new button action (DO SOMETHING!!!)
          switch(m3Type[count]) {
            case 0: 
              // new momentary button state, update and send.
              m3Val[count] = thisVal;
              Serial.print("MUX3-"); 
              Serial.print(count); 
              Serial.print(": "); 
              Serial.println(m3Val[count]);
              sendMIDImessage(m3cc[count], m3Val[count]);
              break;
            case 1: 
              // toggle - check if current button val is on or off & update if needed
              if(thisVal == 127) {
                if(m3Val[count] == 0) {
                  m3Val[count] = 127;
                } else {
                  m3Val[count] = 0;
                }
                Serial.print("MUX3-"); 
                Serial.print(count); 
                Serial.print(": "); 
                Serial.println(m3Val[count]);   
                sendMIDImessage(m3cc[count], m3Val[count]);
              }
              break;
            case 2: 
              // continuous control, update and send
              m3Val[count] = thisVal;
              Serial.print("MUX3-");
              Serial.print(count);
              Serial.print(": ");
              Serial.println(m3Val[count]);
              sendMIDImessage(m3cc[count], m3Val[count]);
              break;
          }
          m3Last[count] = thisVal;
        }
        break;
      case 4: // reading MUX4
        thisVal = translate(analogRead(MUX4), m4Type[count]);
        if(thisVal != m4Last[count]) { 
          
          // new button action (DO SOMETHING!!!)
          switch(m4Type[count]) {
            case 0: 
              // new momentary button state, update and send.
              m4Val[count] = thisVal;
              Serial.print("MUX4-"); 
              Serial.print(count); 
              Serial.print(": "); 
              Serial.println(m4Val[count]);
              sendMIDImessage(m4cc[count], m4Val[count]);
              break;
            case 1: 
              // toggle - check if current button val is on or off & update if needed
              if(thisVal == 127) {
                if(m4Val[count] == 0) {
                  m4Val[count] = 127;
                } else {
                  m4Val[count] = 0;
                }
                Serial.print("MUX4-"); 
                Serial.print(count); 
                Serial.print(": "); 
                Serial.println(m4Val[count]);   
                sendMIDImessage(m4cc[count], m4Val[count]);
              }
              break;
            case 2: 
              // continuous control, update and send
              m4Val[count] = thisVal;
              Serial.print("MUX4-");
              Serial.print(count);
              Serial.print(": ");
              Serial.println(m4Val[count]);
              sendMIDImessage(m4cc[count], m4Val[count]);
              break;
          }
          m4Last[count] = thisVal;
        }
        break;
      default:
        break;
    }


//    thisVal = analogRead(mux);
//    Serial.print(thisVal);
//    Serial.print(" "); 
  }
}

uint8_t translate(int val, int type) {
  // inputs: 
  //    val:      input value (0 - 4095)
  //    type:     0 (momentary) / 1 (toggle) / 2 (continuous)
  //    lastVal:  last output val (check for change for type 1)

  uint8_t translatedValue;
  
  switch(type) {
    case 0: 
      if(val >=2047) {
        translatedValue = 127;
      } else {
        translatedValue = 0;
      }
      return translatedValue; 
    case 1: 
      if(val >=2047) {
        translatedValue = 127;
      } else {
        translatedValue = 0;
      }
      return translatedValue;
    case 2: 
      translatedValue = uint8_t(round(map(val, 0, 4095, 0, 127)));
//      Serial.println(translatedValue);
      return translatedValue;
  }
}

void sendMIDImessage (uint8_t cc, uint8_t val) {
  if (deviceConnected) {
    midiPacket[3] = cc;
    midiPacket[4] = val;

    pCharacteristic->setValue(midiPacket, 5); // packet, length in bytes
    pCharacteristic->notify();
    Serial.print("Sending MIDI: ");
    for(int i=0;i<3;i++) {
      Serial.print(midiPacket[i+2]); 
      Serial.print(" "); 
    }
    Serial.println(); 
  } else {
    Serial.println("BLE not connected!!");
  }
}

//define a callback for key presses
TrellisCallback blink(keyEvent evt){
  // Check is the pad pressed?
  if (trType[evt.bit.NUM] == 0) { // momentary buttons
    if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
      trellis.pixels.setPixelColor(evt.bit.NUM, Wheel(map(evt.bit.NUM, 0, trellis.pixels.numPixels(), 0, 255))); //on rising
      trVal[evt.bit.NUM] = 127; 
    } else if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING) {
    // or is the pad released?
      trellis.pixels.setPixelColor(evt.bit.NUM, 0); //off falling
      trVal[evt.bit.NUM] = 0; 
    }
  } else if (trType[evt.bit.NUM] == 1) { // on/off buttons
    // latching buttons
    if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) { // if button down
      if (trVal[evt.bit.NUM] == 0) { // and current value is 0
        trellis.pixels.setPixelColor(evt.bit.NUM, 0xff0000); //on rising
        trVal[evt.bit.NUM] = 127;
      } else {
        trellis.pixels.setPixelColor(evt.bit.NUM, 0);
        trVal[evt.bit.NUM] = 0;
      }
    }
    
  }

  // Turn on/off the neopixels!
  trellis.pixels.show();

  return 0;
}

// Input a value 0 to 255 to get a color value.
// The colors are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) {
   return trellis.pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   return trellis.pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170;
   return trellis.pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  return 0;
}
