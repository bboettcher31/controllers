#include "Adafruit_NeoTrellis.h"

#define MODULE 0  // 0 == LEFT
                  // 1 == RIGHT

#define MUX1 34
#define MUX2 32
#define MUX3 33
#define LEDPIN 5

// digital select pins
#define S0 4
#define S1 0
#define S2 2

uint32_t thisVal = 0;

long prevMillis = 0;
long currMillis = 0;
bool ledState = false;

int r0 = 0; // value of select pin at the 4051 (s0)
int r1 = 0; // value of select pin at the 4051 (s1)
int r2 = 0; // value of select pin at the 4051 (s2)

int count = 0; // which y pin we are selecting

Adafruit_NeoTrellis trellis; // create a Trellis object

// control values
uint8_t m1vals[] = {0,0,0,0,0,0,0,0};
uint8_t m2vals[] = {0,0,0,0,0,0,0,0};
uint8_t m3vals[] = {0,0,0,0,0,0,0,0};
bool trellisVals[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

// MIDI CC#s
uint8_t m1cc[] = {0,0,0,0,0,0,0,0};
uint8_t m2cc[] = {0,0,0,0,0,0,0,0};
uint8_t m3cc[] = {0,0,0,0,0,0,0,0};
uint8_t trelliscc[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};

// input type (0 = momentary button, 1 = on/off, 2 = continuous)
uint8_t m1type[] = {0,0,0,0,0,0,0,0};
uint8_t m2type[] = {0,0,0,0,0,0,0,0};
uint8_t m3type[] = {0,0,0,0,2,2,2,2};
uint8_t trellistype[] = {0,0,0,0,0,0,0,0};

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);

  pinMode(LEDPIN, OUTPUT);
  pinMode(MUX1, INPUT);
  pinMode(MUX2, INPUT);
  pinMode(MUX3, INPUT);

  if (MODULE == 1) {

    for(int i=0; i<8; i++) {m1type[i] = 2;}
    
    if (!trellis.begin()) {
      Serial.println("Could not start trellis, check wiring?");
      while(1);
    } else {
      Serial.println("NeoPixel Trellis started");
    }
  
    //activate all keys and set callbacks
    for(int i=0; i<NEO_TRELLIS_NUM_KEYS; i++){
      trellis.activateKey(i, SEESAW_KEYPAD_EDGE_RISING);
      trellis.activateKey(i, SEESAW_KEYPAD_EDGE_FALLING);
      trellis.registerCallback(i, blink);
    }
  
    //do a little animation to show we're on
    for (uint16_t i=0; i<trellis.pixels.numPixels(); i++) {
      trellis.pixels.setPixelColor(i, Wheel(map(i, 0, trellis.pixels.numPixels(), 0, 255)));
      trellis.pixels.show();
      delay(50);
    }
    for (uint16_t i=0; i<trellis.pixels.numPixels(); i++) {
      trellis.pixels.setPixelColor(i, 0x000000);
      trellis.pixels.show();
      delay(50);
    }
  }
  
  Serial.println("Bionic Harp test"); 
  if (MODULE == 0) {
    Serial.println("LEFT module");
  } else {
    Serial.println("RIGHT module");
  }
  for(int i=0; i<5; i++){
    digitalWrite(LEDPIN, HIGH);
    Serial.print(". ");
    delay(200);
    digitalWrite(LEDPIN, LOW);
    delay(200);
  }
  Serial.println("");
}

void loop() {
  // put your main code here, to run repeatedly:

  currMillis = millis();
  if ( currMillis > prevMillis + 333) {
    if (ledState) {
      digitalWrite(LEDPIN, LOW);
      ledState = false;
    } else {
      digitalWrite(LEDPIN, HIGH);
      ledState = true;
    }
    prevMillis = currMillis;
  }

  if (MODULE == 0) {
    readMux(MUX1); 
    Serial.print(" - "); 
    readMux(MUX2); 
    Serial.print(" - ");
    readMux(MUX3);
  } else {
    readMux(MUX1); 
    Serial.print(" - ");
    trellis.read();
    for(int i=0; i<16; i++) {
      Serial.print(trellisVals[i]); 
      Serial.print(" ");
    }
  }
  
  Serial.println("");
  /*
  knobVal = analogRead(POT); 
  Serial.println(knobVal);
  */
  
  delay(100);
}

void readMux(int mux) {
  for(count=0; count<=7; count++) {

    r0 = bitRead(count, 0);
    r1 = bitRead(count, 1);
    r2 = bitRead(count, 2);

    digitalWrite(S0, r0);
    digitalWrite(S1, r1);
    digitalWrite(S2, r2);

    thisVal = analogRead(mux);
    Serial.print(thisVal);
    Serial.print(" "); 
  }
}

//define a callback for key presses
TrellisCallback blink(keyEvent evt){
  // Check is the pad pressed?
  if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING) {
    trellis.pixels.setPixelColor(evt.bit.NUM, Wheel(map(evt.bit.NUM, 0, trellis.pixels.numPixels(), 0, 255))); //on rising
    trellisVals[evt.bit.NUM] = 1; 
//    Serial.println();
//    Serial.print(evt.bit.NUM); 
//    Serial.println(" 1"); 
  } else if (evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING) {
  // or is the pad released?
    trellis.pixels.setPixelColor(evt.bit.NUM, 0); //off falling
    trellisVals[evt.bit.NUM] = 0; 
//    Serial.println();
//    Serial.print(evt.bit.NUM); 
//    Serial.println(" 0"); 
  }

  // Turn on/off the neopixels!
  trellis.pixels.show();

  return 0;
}

// Input a value 0 to 255 to get a color value.
// The colors are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) {
   return trellis.pixels.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   return trellis.pixels.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170;
   return trellis.pixels.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  return 0;
}
